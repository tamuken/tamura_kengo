<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>編集画面</title>
        <link href="css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

            <form action="settings" method="post"><br />

            	<input name="id" value="${editUser.id}" id="id" type="hidden" />
			<input type="hidden" name="id" value="${editUser.id }">
    			<span class="loginid"><c:out value="${editUser.loginid }の編集画面" /></span><br><br />

                <input name="id" value="${editUser.id}" id="id" type="hidden"/>

                <label for="loginid">ログインID</label>
                <input name="loginid" value="${editUser.loginid}" id="loginid"/><br />

                <label for="name">名前</label>
                <input name="name" value="${editUser.name}" id="name"/><br />

                <label for="password">パスワード</label>
                <input name="password" type="password" id="password"/> <br />

                <label for="conPassword">確認用パスワード</label>
				<input name="conPassword" type="password" id="conPassword" /> <br />

			<label for="branch">支店名</label> <select name="branch" id="branch">
			<c:choose>
				<c:when test="${editUser.branch==1}">
				<option selected value="1">本社</option>>
				<option value="2">支店A</option>
				<option value="3">支店B</option>
				<option value="4">支店C</option>
				</c:when>
				<c:when test="${editUser.branch==2}">
				<option value="1">本社</option>
				<option selected value="2">支店A</option>>
				<option value="3">支店B</option>
				<option value="4">支店C</option>
				</c:when>
				<c:when test="${editUser.branch==3}">
				<option value="1">本社</option>
				<option value="2">支店A</option>>
				<option selected value="3">支店B</option>>
				<option value="4">支店C</option>
				</c:when>
				<c:when test="${editUser.branch==4}">
				<option value="1">本社</option>
				<option value="2">支店A</option>>
				<option value="3">支店B</option>>
				<option selected value="4">支店C</option>>
				</c:when>
			</c:choose>
			</select> <br />
			<label for="department">部署・役職</label> <select name="department" id="department">
			<c:choose>
				<c:when test="${editUser.department==1}">
				<option selected value="1">総務人事担当者</option>>
				<option value="2">情報管理担当者</option>
				<option value="3">支店長</option>
				<option value="4">社員</option>
				</c:when>
				<c:when test="${editUser.department==2}">
				<option value="1">総務人事担当者</option>
				<option selected value="2">情報管理担当者</option>>
				<option value="3">支店長</option>
				<option value="4">社員</option>
				</c:when>
				<c:when test="${editUser.department==3}">
				<option value="1">総務人事担当者</option>
				<option value="2">情報管理担当者</option>>
				<option selected value="3">支店長</option>>
				<option value="4">社員</option>
				</c:when>
				<c:when test="${editUser.department==4}">
				<option value="1">総務人事担当者</option>
				<option value="2">情報管理担当者</option>>
				<option value="3">支店長</option>>
				<option selected value="4">社員</option>>
				</c:when>
			</c:choose>
			</select> <br />
                <input type="submit" value="登録" /> <br />
                <a href="./">戻る</a>
            </form>
            <div class="copyright"> Copyright(c)Your Name</div>
        </div>
    </body>
</html>