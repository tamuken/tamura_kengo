<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>tamura_kengo</title>
    </head>
    <body>
    <div class="header">
        <a href="signup">登録する</a>
    </div>
    <div class="userList">
 <table border="1">
    <tr>
      <th>ログインID</th>
      <th>名前</th>
      <th>支店</th>
      <th>部署・役職</th>
      <th>登録情報編集</th>
    </tr>
        <c:forEach items="${userList}" var="userList">
                <tr>
                    <td class="loginid"><c:out value="${userList.loginid}" /></td>
                    <td class="name"><c:out value="${userList.name}" /></td>
               		<td class="branch"><c:out value="${userList.branch_name}" /></td>
                	<td class="department"><c:out value="${userList.department_name}" /></td>
                	<td>
                	<form action="settings" method="GET">
                	<input type="hidden" name="id" value="${userList.id}" />
                	<input type="submit" value="編集" />
                	</form>
                	<form action="index.jsp" method="POST">
                	<c:choose>
                	<c:when test="${userList.stop==1}">
                	<input type="hidden" name="id" value="${userList.id}" />
					<input type="hidden" name="stop" value="0" />
					<input type="submit" value="停止" onClick="return confirm('アカウントを停止しますか？')" />

					</c:when>
					<c:when test="${userList.stop==0}">
                	<input type="hidden" name="id" value="${userList.id}" />
					<input type="hidden" name="stop" value="1" />
					<input type="submit" value="復活" onClick="return confirm('アカウントを復活しますか？')"/>
					</c:when>
					</c:choose>
					</form>
                	</td>
				</tr>
    </c:forEach>
  </table>
  </div>


<body>
      <div class="userList"> Copyright(c)YourName</div>


</html>