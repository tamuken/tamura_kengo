package chapter7.service;

import static chapter7.utils.CloseableUtil.*;
import static chapter7.utils.DBUtil.*;

import java.sql.Connection;

import chapter7.beans.User;
import chapter7.dao.SettingUserDao;
import chapter7.utils.CipherUtil;

public class SettingService {

	public User getUser(int id) {

		Connection connection = null;
		try {
			connection = getConnection();

			SettingUserDao userDao = new SettingUserDao();
			User user = userDao.getUser(connection, id);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public User update(User editUser) {

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(editUser.getPassword());
			editUser.setPassword(encPassword);
			SettingUserDao settingUserDao = new SettingUserDao();
			User user = settingUserDao.update(connection, editUser);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}

	public User update2(User editUser) {
		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(editUser.getPassword());
			editUser.setPassword(encPassword);
			SettingUserDao settingUserDao = new SettingUserDao();
			User user = settingUserDao.update2(connection, editUser);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}

	/**
	 * ログインID重複チェック
	 */
	public boolean checkId(String id, String loginid) {
		Connection connection = null;
		try {
			connection = getConnection();

			SettingUserDao userdao = new SettingUserDao();
			boolean checkId = userdao.checkedId(connection, id, loginid);

			commit(connection);

			return checkId;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
