package chapter7.dao;

import static chapter7.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import chapter7.beans.User;
import chapter7.exception.NoRowsUpdatedRuntimeException;
import chapter7.exception.SQLRuntimeException;

public class SettingUserDao {

	public User getUser(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id, ");
			sql.append("users.loginid, ");
			sql.append("users.name, ");
			sql.append("users.branch, ");
			sql.append("users.department ");
			sql.append("FROM users ");
			sql.append("Where id =" + id);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			User ret = toUser(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private User toUser(ResultSet rs)
			throws SQLException {

		User ret = new User();
		while (rs.next()) {
			int id = rs.getInt("id");
			String loginid = rs.getString("loginid");
			String name = rs.getString("name");
			int branch = rs.getInt("branch");
			int department = rs.getInt("department");

			User editUser = new User();
			editUser.setId(id);
			editUser.setLoginid(loginid);
			editUser.setName(name);
			editUser.setBranch(branch);
			editUser.setDepartment(department);
			return editUser;
		}
		return ret;
	}

	/**
	 * 変更(パスワード変更有の場合)
	 */
	public User update(Connection connection, User editUser) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append("  loginid = ?");
			sql.append(", name = ?");
			sql.append(", password = ?");
			sql.append(", branch = ?");
			sql.append(", department = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, editUser.getLoginid());
			ps.setString(2, editUser.getName());
			ps.setString(3, editUser.getPassword());
			ps.setInt(4, editUser.getBranch());
			ps.setInt(5, editUser.getDepartment());
			ps.setInt(6, editUser.getId());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}

			return editUser;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	 * 変更(パスワード無しの場合)
	 */
	public User update2(Connection connection, User editUser) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append("  loginid = ?");
			sql.append(", name = ?");
			sql.append(", branch = ?");
			sql.append(", department = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, editUser.getLoginid());
			ps.setString(2, editUser.getName());
			ps.setInt(3, editUser.getBranch());
			ps.setInt(4, editUser.getDepartment());
			ps.setInt(5, editUser.getId());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}

			return editUser;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	 * ログインID重複チェック
	 */
	public boolean checkedId(Connection connection, String id, String loginid) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT id FROM users WHERE loginid = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginid);

			ResultSet rs = ps.executeQuery();
			String ret = null;
			while (rs.next()) {
				ret = rs.getString("id");
				if (ret.equals(id)) {
					return true;
				}
				return false;
			}
			return true;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}
