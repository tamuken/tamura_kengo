package chapter7.dao;

import static chapter7.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import chapter7.beans.User;
import chapter7.exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append(" name");
			sql.append(", loginid");
			sql.append(", password");
			sql.append(", branch");
			sql.append(", department");
			sql.append(") VALUES (");
			sql.append(" ?"); // name
			sql.append(", ?"); // loginid
			sql.append(", ?"); // password
			sql.append(", ?"); //  branch
			sql.append(", ?"); // department
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getName());
			ps.setString(2, user.getLoginid());
			ps.setString(3, user.getPassword());
			ps.setInt(4, user.getBranch());
			ps.setInt(5, user.getDepartment());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	 * ログインID重複確認
	 */
	public int checked(Connection connection, String loginid) {

		PreparedStatement ps = null;
		try {
			String sql = "select loginid from users where loginid =?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginid);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				return 1;
			}
			return 0;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}
