package chapter7.dao;

import static chapter7.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter7.beans.User;
import chapter7.exception.NoRowsUpdatedRuntimeException;
import chapter7.exception.SQLRuntimeException;

public class UserdataDao {

	public List<User> getUser(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("select ");
			sql.append("users.id,");
			sql.append("users.loginid, ");
			sql.append("users.name, ");
			sql.append("branchs.branch_name, ");
			sql.append("departments.department_name, ");
			sql.append("users.stop ");
			sql.append("from users ");
			sql.append("inner join branchs  ");
			sql.append("on users.branch=branchs.id ");
			sql.append("inner join departments ");
			sql.append("on users.department=departments.id ");
			sql.append("GROUP BY id ASC");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<User> ret = toUserDataList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUserDataList(ResultSet rs)
			throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String loginid = rs.getString("loginid");
				String branch = rs.getString("branch_name");
				String department = rs.getString("department_name");
				int stop = rs.getInt("stop");

				User userList = new User();
				userList.setId(id);
				userList.setName(name);
				userList.setLoginid(loginid);
				userList.setBranch_name(branch);
				userList.setDepartment_name(department);
				userList.setStop(stop);

				ret.add(userList);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public static User changed(Connection connection, String id, String stop) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET ");
			sql.append("stop = ? ");
			sql.append(" WHERE ");
			sql.append("id = ? ");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, stop);
			ps.setString(2, id);

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
		return null;
	}
}
