package chapter7.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter7.beans.User;
import chapter7.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<>();

        HttpSession session = request.getSession();
        User user = new User();
        user.setName(request.getParameter("name"));
        user.setLoginid(request.getParameter("loginid"));
        user.setPassword(request.getParameter("password"));
        user.setBranch(Integer.parseInt((request.getParameter("branch"))));
        user.setDepartment(Integer.parseInt((request.getParameter("department"))));

        if (isValid(request, messages) == true) {
            new UserService().register(user);
            response.sendRedirect("./");
        } else {
			request.setAttribute("user", user);
            session.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String name = request.getParameter("name");
        String password = request.getParameter("password");
        String conPassword = request.getParameter("conPassword");
        String loginid = request.getParameter("loginid");

        if (StringUtils.isEmpty(name)){
            messages.add("名前を入力してください");
        }
        if (StringUtils.isEmpty(password)) {
            messages.add("パスワードを入力してください");
        }
        if (!(password.equals(conPassword))){
            messages.add("入力したパスワードと確認用パスワードが一致していません");
        }
        if (StringUtils.isEmpty(conPassword)) {
            messages.add("確認用パスワードを入力してください");
        }
        if (name.length()>10){
        	messages.add("名前は10文字以下で入力してください");
        }
		if ((!loginid.matches("[0-9a-zA-Z]{6,20}"))) {
			messages.add("ログインIDは半角文字6文字以上20文字以下で入力してください");
        }
		if (!(StringUtils.isEmpty(password))) {
			if ((!password.matches("[0-9a-zA-Z]{6,20}"))) {
				messages.add("パスワードは半角文字6文字以上20文字以下で入力してください");
			}
        }
		/**
		 * ログインID重複確認
		 */
        int count = new UserService().check(loginid);
        if (count == 1) {
        	messages.add("入力したログインIDは使用されています。");
        }

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}