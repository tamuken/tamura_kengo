package chapter7.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter7.beans.User;
import chapter7.exception.NoRowsUpdatedRuntimeException;
import chapter7.service.SettingService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		int id = Integer.parseInt(request.getParameter("id"));
		User editUser = new SettingService().getUser(id);
		request.setAttribute("editUser", editUser);

		request.getRequestDispatcher("settings.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setLoginid(request.getParameter("loginid"));
		editUser.setName(request.getParameter("name"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setBranch(Integer.parseInt(request.getParameter("branch")));
		editUser.setDepartment(Integer.parseInt(request.getParameter("department")));

		if (isValid(request, messages) == true) {
			try {
				//パス空白時の処理
				String pass = request.getParameter("password");
				String passcheck = request.getParameter("conPassword");
				if (StringUtils.isBlank(pass) && StringUtils.isBlank(passcheck)) {
					new SettingService().update2(editUser);//pass未変更処理
				} else {
					new SettingService().update(editUser);//pass変更処理
				}

			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");

				HttpSession session = request.getSession();
				session.setAttribute("errorMessages", messages);
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("settings.jsp").forward(request, response);
				return;
			}
			response.sendRedirect("./");
		} else {
			request.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", editUser);
			request.getRequestDispatcher("settings.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String conPassword = request.getParameter("conPassword");
		String loginid = request.getParameter("loginid");
		String id = request.getParameter("id");

		if (StringUtils.isEmpty(name) == true) {
			messages.add("名前を入力してください");
		}
		//		if (!(password.equals(conPassword))) {
		//			messages.add("パスワードが一致していません");
		//		}
		if (!(StringUtils.isEmpty(password) && StringUtils.isEmpty(conPassword))) {
			if (!(password.matches("[0-9a-zA-Z]{6,20}"))) {
				messages.add("パスワードは半角文字で6文字以上20文字以下で入力してください");
			} else if (!(password.equals(conPassword))) {
				messages.add("入力したパスワードと確認用パスワードが一致していません");
			}
		}
		if (!(StringUtils.isEmpty(password))) {
			if (StringUtils.isEmpty(conPassword) == true) {
				messages.add("確認用パスワードを入力してください");
			}
		}
		if (name.length() > 10) {
			messages.add("名前は10文字以下で入力してください");
		}
		if (StringUtils.isEmpty(loginid)) {
			messages.add("ログインIDを入力してください");
		} else if (!(loginid.matches("[a-zA-Z0-9]{6,20}"))) {
			messages.add("ログインIDは半角英数字で6文字以上20文字以下で入力してください");
		}
		//		if ((!loginid.matches("[0-9a-zA-Z]{6,20}"))) {
		//			messages.add("ログインIDは半角文字6文字以上20文字以下で入力してください");
		//		}
		if (!(StringUtils.isEmpty(password))) {
			if ((!password.matches("[0-9a-zA-Z]{6,20}"))) {
				messages.add("パスワードは半角文字6文字以上20文字以下で入力してください");
			}
		}
		/**
		 * ログインID重複チェック
		 */
		boolean idcheck = new SettingService().checkId(id, loginid);
		if (idcheck != true) {
			messages.add("入力したログインIDは使用されています。");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}