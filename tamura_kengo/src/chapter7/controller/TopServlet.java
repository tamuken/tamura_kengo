package chapter7.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter7.beans.User;
import chapter7.service.DataService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		List<User> userList = new DataService().getMessage();
		request.setAttribute("userList", userList);
		request.getRequestDispatcher("/top.jsp").forward(request, response);
	}

	@Override
	/**
	 * アカウント停止復活
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		String id = request.getParameter("id");
		String stop = request.getParameter("stop");

		new DataService().chenge(id, stop);

		response.sendRedirect("./");
	}
}